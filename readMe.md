## VOD Movies Scrapper

This project scrapes "all" movies from Netflix based on the main netflix genres.

### Expected outcome
- Return all movies title,
- Return movies description,
- Return Type
- Return url, Content ID or slug


### Current outcome
- Retrieve all movie title,
- Retrieve url, of movies

### How do you use this app?

1. If you do not have Node.js, download and install it.
2. Clone the repository.
3. Install dependencies, with yarn install, note not npm install
4. Use "yarn start" to run the app