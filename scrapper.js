const puppeteer = require("puppeteer");

const netflixLoginPage = "https://www.netflix.com/login";
const netflixMoviesBaseUrl = 'https://www.netflix.com/browse/genre/';
const netflixMoviesPage ='https://www.netflix.com/browse/genre/34399';
const netflixMoviesExtraParams = '?so=az';

let results = [];


async function scrape(user) {
  console.log("Starting scrape...");
  console.log("Launching headless browser...");
  const browser = await puppeteer.launch({ headless: false });
  let page = await browser.newPage();

  await page.setViewport({ width: 1280, height: 800 });

  // Open login page and login
  console.log("Loading Netflix login page...")
  await page.goto(netflixLoginPage);
  await page.type('#id_userLoginId',user.username );
  await page.type('#id_password', user.password);
  await page.keyboard.press('Enter')
  console.log("Logging in to Netflix...");
  await page.waitForNavigation({ waitUntil: 'networkidle2', timeout: 0 });
  
  await clickUserProfile(page, user);
  

  console.log('Loading genres')
    await page.goto(netflixMoviesPage, { waitUntil: 'networkidle2', timeout: 0 });
  
  // Click the 'Genres' drop-down menu
  await page.click('div[label="Genres"] > div');
  console.log("Loading movie genres...");

  const codes = await page.evaluate(() => {
      let anchors = document.querySelectorAll('div[label="Genres"] > div + div li > a');
      console.log(anchors)
      let codes = [];

      for (let a of anchors) {
          let genre = {
              name: a.innerText,
              code: a.pathname.substr(a.pathname.lastIndexOf('/') + 1)
          }

          codes.push(genre);
      }
      return codes;
  });

  for (let c of codes) {
      console.log("Loading genre: " + c.name + " - " + c.code);
      await page.goto(netflixMoviesBaseUrl + c.code + netflixMoviesExtraParams, { waitUntil: 'networkidle2', timeout: 0 });
      let profileSelectionRequired = await page.evaluate(() => {
          let profileDiv = document.querySelector('.list-profiles');
          if (profileDiv === null) {
              return false;
          } else {
              return true;
          }
      });

      if (profileSelectionRequired) {
          console.log("User profile needed...");
          await clickUserProfile(page, user);
      } 

      results = await scrapeMovies(page, extractItems, results, user);
  }

  console.log("Completed scraping Netflix movies.");

  browser.close();

  console.log(results)

  return results;
}

function extractItems() {
    const extractedElements = document.querySelectorAll('p.fallback-text');
    const items = [];
    extractedElements.forEach(function (element) {
        items.push(element.innerText);
    });
    return items;
}

async function scrapeMovies(
    page,
    extractItems,
    results,
    user,
    scrollDelay = 1000,
) {
    console.log("Scraping page.");

    let movieTitles = [];
    try {
        let previousHeight;
        let  movieDescription ;
        while (movieTitles.length < 1000000000000) {
            movieTitles = await page.evaluate(extractItems);
        //     movieDescription= await page.evaluate(document.querySelector('p.preview-modal-synopsis.previewModal--text').innerText);
        //    console.log(movieDescription)
            previousHeight = await page.evaluate('document.body.scrollHeight');
            await page.evaluate('window.scrollTo(0, document.body.scrollHeight)');
            await page.waitForFunction(`document.body.scrollHeight > ${previousHeight}`, { timeout: 3000 });
            await page.waitFor(scrollDelay);
        }
    } catch (e) { }

    if (movieTitles.length === 0) {
        console.log("No movies in this category.");
    }

    for (let i = 0; i < movieTitles.length; i++) {
        if (!movieExists(results, movieTitles[i])) {
            let movie = new Object();
            movie.title = movieTitles[i];

            results.push(movie)
        }
    }


    return results;
}

function movieExists(arrOfMovieObjects, nameOfMovie) {
    return arrOfMovieObjects.some(movieObject => nameOfMovie === movieObject.title);
}

async function clickUserProfile(page, user) {

    const indexOfUserAccount = await page.evaluate((profile) => {
       
        let userAccounts = document.querySelector("#appMountPoint>div.netflix-sans-font-loaded>div>div>div.bd.dark-background>div.profiles-gate-container>div>div.list-profiles>.choose-profile").children;
        // Default user profile index.
        let index = 0;

        for (let i = 0; i < userAccounts.length; i++) {
            if (userAccounts[i].innerText === profile) {
                index = i;
                break;
            }
        }

        return index + 1;
    }, user.profile);

    // Click the user account if the user entered a correct account name. Otherwise, choose 1st account.
    await page.click('li.profile:nth-child(' + indexOfUserAccount + ') > div:nth-child(1) > a:nth-child(1)');
    console.log("Loading user profile...");
    // await page.waitForNavigation({ waitUntil: 'networkidle2', timeout: 0 });

    

}
module.exports = scrape;